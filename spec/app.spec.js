var Request = require("request");

describe("Server", () => {
    var aux;
    beforeAll(() => {
        aux = require("../app/app");
    });
    afterAll(() => {
        aux.close();
    });
    describe("GET /", () => {
        var data = {};
        beforeAll((done) => {
            Request.get("http://localhost:3000/", (error, response, body) => {
                data.status = response.statusCode;
                data.body = body;
                done();
            });
        });
        it("Status 200", () => {
            console.log("error")
            console.log(data.status)
            expect(data.status).toBe(200);
        });
        it("Body", () => {
            expect(data.body).toEqual("Software Development Fundamentals");
        });
        
        // it("should connect", () =>{
        //     expect(data.s)
        //     spyOn(server, "connecDB")
        // })

        

    });
    describe("GET /test", () => {
    var data = {};
    beforeAll((done) => {
    Request.get("http://localhost:3000/test", (error, response, body) => {
    data.status = response.statusCode;
    data.body = JSON.parse(body);
    done();
    });
    });
    it("Status 200", () => {
        expect(data.status).toBe(500);
    });

    it("No to be null", () => {
        expect(data.status).not.toBeNull();
    });
    it("message not to be null", () => {
        expect(data.message).not.toBeNull();
    })
    
    it("Body", () => {
        expect(data.body.message).toBe("This is an error response");
    });
    it("Error message", () =>{
        expect(data.body.message).toContain("error")
    })
    it("Matching status", () => {
        expect(data.body.message).toMatch("This is an error response")
        
    })
    
    });
});